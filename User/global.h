#include "constantes.h" // fichier contenant toutes les constantes du projet
#include <stdint.h>


// mettez ici toutes les "extern" correspondant aux d�clarations contenues dans globadec.h

extern char chaine[30]; // buffer pour l'affichage sur le LCD
extern uint16_t touch_x, touch_y ;

extern int nbfautes = 0;

// Variables pour l'ecriture dans la memoire
extern uint8_t ADDR = 0x0A; // ...1010 bits de poids de ADDR
extern uint8_t arr[] = {0x41,0x4C,0x47,0X4F,0X52,0x49,0X54,0x48,0x4D,0x45,0x53,0x55,0x50,0x4E,0x43}; // Exemple de donnes  envoyer ALGORITHMESUPNC
extern const uint16_t size_arr = 15;
extern const int size_copie_arr = 32;
extern uint8_t copieArr[size_copie_arr];
extern int x_debut;
extern int nbfautes;
extern int longueur_totale_cases;
