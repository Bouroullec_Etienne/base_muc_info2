#include "vrai_test_bth.h"
#include "globaldec.h"


// Déclaration du buffer de réception
char buffer[255] = {0};

// Fonction d'initialisation du port série
void initSerial2() {
  // Configuration du port série
  UART_CFG_Type UARTConfigStruct;
	PINSEL_CFG_Type PinCfg;

  PinCfg.Funcnum = 1;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Pinnum = 2;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 3;
	PINSEL_ConfigPin(&PinCfg);

  UART_ConfigStructInit(&UARTConfigStruct);

	// Re-configure baudrate to 115200bps
	UARTConfigStruct.Baud_rate = 38400;

	// Initialize DEBUG_UART_PORT peripheral with given to corresponding parameter
	UART_Init(LPC_UART0, &UARTConfigStruct);

	// Enable UART Transmit
	UART_TxCmd(LPC_UART0, ENABLE);
}


void UARTPutChar (LPC_UART_TypeDef *UARTx, uint8_t ch)
{
	UART_Send(UARTx, &ch, 1, BLOCKING);
}



// Fonction d'envoi d'une chaîne de caractères
void sendString(const void *str)
{
	uint8_t *s = (uint8_t *) str;

	while (*s)
	{
		UARTPutChar(LPC_UART0, *s++);
	}
}

// Fonction de réception d'un caractères
uint8_t UARTGetChar ()
{
	uint8_t tmp;
	UART_Receive(LPC_UART0, &tmp, 1, BLOCKING);
	return(tmp);
}

void UART0_IRQHandler(void){
		 lettrerecue_bth = UARTGetChar();
			sendString(&lettrerecue_bth);
}


// Fonction principale
void testbth() {
	
		UART_IntConfig(LPC_UART0, UART_INTCFG_RBR, ENABLE);	//Interruption Enable
		NVIC_EnableIRQ(UART0_IRQn); // Activer l'interruption UART0 dans le NVIC

    // Ligne reçue avec succès
    //sendString("Ligne reçue : ");
    //sendString("\n");
 }





