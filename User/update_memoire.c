#include "update_memoire.h"
I2C_M_SETUP_Type masterType;
GPIO_Byte_TypeDef pGPIO;
PINSEL_CFG_Type pPINSEL;
I2C_M_SETUP_Type TransferCfg;
I2C_TRANSFER_OPT_Type optType;

// Configuration de SDA
void config_SDA() {
    pPINSEL.Portnum = PINSEL_PORT_0;
    pPINSEL.Pinnum = PINSEL_PIN_27;
    pPINSEL.Funcnum = 1;
}

// Configuration de SCL
void config_SCL() {
    pPINSEL.Portnum = PINSEL_PORT_0;
    pPINSEL.Pinnum = PINSEL_PIN_28;
    pPINSEL.Funcnum = 1;
}

int config_I2C(uint8_t adresse_et_position_page) {
    PINSEL_ConfigPin(&pPINSEL); // Configurer Pinsel
    config_SDA();
    config_SCL();
    I2C_Init(LPC_I2C0, 400000); // 400000 = fréquence
    I2C_Cmd(LPC_I2C0, ENABLE);

    // Décalage à gauche de 4 bits
    ADDR = (ADDR << 4); // masterType.sl_addr7bit = ...1010 => ADDR = 1010...

    masterType.sl_addr7bit = ADDR | ((adresse_et_position_page & 0x700) >> 7); // ADDR | Retirer la position de la page et la décaler de 3

    return masterType.sl_addr7bit;
}

void configMaster_write(uint8_t adresse_et_position_page) {
    int i, j, k;
    int length = 0;
    char* word;

    k = 1;
    TransferCfg.sl_addr7bit = (config_I2C(adresse_et_position_page)) >> 1; // >> 1 car la case R-W est déjà réservée

    copieArr[0] = (adresse_et_position_page & 0xFF);
    for (i = 0; i < number_of_words; i++) {
        word = arr[i];
        for (j = 0; j < strlen(word); j++) {
            copieArr[k++] = word[j];
        }
    }
    length = k; 

    TransferCfg.tx_data = copieArr;
    TransferCfg.tx_length = length; // Longueur totale de la data envoyée
    TransferCfg.rx_data = NULL;
    TransferCfg.rx_length = 0;
    TransferCfg.retransmissions_max = 5;
    I2C_MasterTransferData(LPC_I2C0, &TransferCfg, optType);
}

void write_I2C() {
    configMaster_write((uint8_t) 505); // écrire dans la page spécifiée
}
