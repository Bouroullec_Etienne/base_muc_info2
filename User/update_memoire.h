#include "globaldec.h"

#include <string.h>
#include <stdio.h>

void config_SDA();
void config_SCL();
int config_I2C(uint8_t);
void configMaster_write(uint8_t);
void write_I2C();