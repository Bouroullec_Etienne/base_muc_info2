#include <lpc17xx.h>
#include "string.h"
#include "lpc17xx_clkpwr.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_timer.h"

// Définition de la vitesse de transmission
#define BAUD_RATE 38400

// Définition de la fréquence du CPU
#define F_CPU 12000000

void initSerial2();
void UARTPutChar (LPC_UART_TypeDef *UARTx, uint8_t ch);
uint8_t UARTGetChar ();
void sendString(const void *str);
int receiveLine(char *buffer, int size);
void testbth();
void UART0_IRQHandler(void);