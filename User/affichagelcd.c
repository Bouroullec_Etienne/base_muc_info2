#include "touch/ili_lcd_general.h"
#include "touch/lcd_api.h"
#include "touch/touch_panel.h"
#include "global.h"
#include "globaldec.h"

void dessiner_ligne(unsigned int x, unsigned int y, unsigned int l,unsigned int e, char orientation, unsigned short color)
{
	int i,j;
	if(orientation=='v')
	{
		for(j=y;j<=y+l;j++)
		{
			lcd_SetCursor(x,j);//on place le curseur � la bonne position
			rw_data_prepare();
			for(i=0;i<=e;i++)
			{
				write_data(color);//on trace un point et on passe � la position suivante
			}
		}
	}
	else//orientation='h'
	{
		for(j=y;j<=y+e;j++)
		{
			lcd_SetCursor(x,j);//on place le curseur � la bonne position
			rw_data_prepare();
			for(i=0;i<=l;i++)
			{
				write_data(color);//on trace un point et on passe � la position suivante
			}
		}
	}
}

void dessiner_rect(unsigned int x, unsigned int y, unsigned int lng, unsigned int lrg, unsigned int e, unsigned short plein, unsigned short e_color, unsigned short bg_color)
{
	//dessiner fond
	if(plein==1)
	{
		dessiner_ligne(x,y,lng,lrg,'h',bg_color);
	}
	
	//dessiner bordures
	dessiner_ligne(x,y,lng,e,'h',e_color);
	dessiner_ligne(x+lng-e,y,lrg,e,'v',e_color);
	dessiner_ligne(x,y+lrg-e,lng,e,'h',e_color);
	dessiner_ligne(x,y,lrg,e,'v',e_color);
}
void dessiner_une_lettre(char x,int position){
	switch x{
	case 'a':
		dessiner_lettre_A(position,240,Black);
		break;
	case 'l':
		dessiner_lettre_L(position,240,Black);
		break;
	case 'g':
		dessiner_lettre_G(position,240,Black);
		break;
	case 'o':
		dessiner_lettre_O(position,240,Black);
		break;
	case 'r':
		dessiner_lettre_R(position,240,Black);
		break;
	case 'i':
		dessiner_lettre_I(position,240,Black);
		break;
	case 't':
		dessiner_lettre_T(position,240,Black);
		break;
	case 'h':
		dessiner_lettre_H(position,240,Black);
		break;
	case 'm':
		dessiner_lettre_M(position,240,Black);
		break;
	case 'e':
		dessiner_lettre_E(position,240,Black);
		break;
	case 'u':
		dessiner_lettre_U(position,240,Black);
		break;
	case 's':
		dessiner_lettre_S(position,240,Black);
		break;
	case 'p':
		dessiner_lettre_P(position,240,Black);
		break;
	case 'n':
		dessiner_lettre_N(position,240,Black);
		break;
	case 'c':
		dessiner_lettre_C(position,240,Black);
		break;
	}

	
}


void dessiner_bonhomme(){
	//On dessine la tête
	dessiner_rect(120,75,10,10,1,1,Black,Black);
	//On dessine le ventre
	dessiner_rect(120,85,10,15,1,1,Black,Black);
	//On dessine la jambe droite
	dessiner_rect(120,100,3,20,1,1,Black,Black);
	//On dessine la jambe gauche
	dessiner_rect(125,100,3,20,1,1,Black,Black);
	//Il manque les bras
	//On dessine le gauche
	dessiner_rect(110,90,10,5,1,1,Black,Black);
	//On dessine le droit
	dessiner_rect(130,90,10,5,1,1,Black,Black);
}

void dessiner_pendouilloire(){
	switch (nbfautes)
	{
		case 0:
		break;
		case 1:
	//Dessiner la premiere barre
		dessiner_rect(53,110,40,5,1,1,Black,Black);
		break;
		case 2:
	//Dessiner la deuxieme barre
		dessiner_rect(70,50,5,60,1,1,Black,Black);
		break;
		case 3:
	//Dessiner la troisieme barre
		dessiner_rect(70, 50 ,60 , 10 ,1,1, Black,Black);
		break;
		case 4:
	//Dessiner la quatireme barre
		dessiner_rect(120, 50, 10, 17,1,1,Black, Black);
		break;
		//On dessine la corde
		case 5:
		dessiner_rect(124,65,3,10,1,1,Black,Black);
	default:
		break;
	}
}


//

// Maintenant pour afficher les lettres ils faut bien coder la façon dont les lettres vont apparaitres à l'ecran;
// Pour se faire on a prévu à l'avance les mots que l'utilisateur allait devoir deviner
// Il s'agit des mots Alogrithme, Reseau, Photon, Octet, Laser
// On va donc devoir faire des fonction d'affichages pour chacune des lettres
// Donc pour les lettes : A L G O R I T H M E S U P N C
// Un problème se présente, les mots ne font pas tous la meme longueur, pour dessiner on ne vas pas pouvoir les dessiner a des coordonnées fixes;
// C'est pourquoi chacune de ces fonctions doit prendre en argument un emplacement, pour savoir ou commencer a dessiner;



void afficher_lettre_C(unsigned int x, unsigned int y, unsigned short color) {
    // Dessiner le triangle supérieur de la lettre "A"
    dessiner_ligne(x + 10, y, 100, 5, 'h', color); // Ligne horizontale supérieure
    dessiner_ligne(x + 5, y + 50, 20, 5, 'h', color); // Ligne horizontale inférieure gauche
    dessiner_ligne(x + 90, y + 50, 20, 5, 'h', color); // Ligne horizontale inférieure droite

    // Dessiner les côtés du triangle supérieur
    dessiner_ligne(x, y, 50, 5, 'v', color); // Ligne verticale gauche
    dessiner_ligne(x + 110, y, 50, 5, 'v', color); // Ligne verticale droite
}

// Cette procédure va dessiner les cases présentes en dessous de du bonhomme pendu 
// C ne supporte pas les String donc on doit passer par du char[ longueur du mot], FOndamentalement cela ne change pas grand chose,
// car il fautjuste connaitre la longueur du mot dans dessiner_case
void dessiner_cases(char mot[]) {
    int longueur = strlen(mot); // On prends la longueur du mot
    int espace_entre_cases = 10; // Espacement entre chaque cases
    longueur_totale_cases = longueur * 20 + (longueur - 1) * espace_entre_cases; // Calcul de la longueur totale des cases
	//Cela represente la longueur que vont prendre toutes les cases en comptant les espcaces entre chacune des cases.
	//Chaque case fait 20 pixels de depart puis on multiplie par le nombre de cases donc ici la longueur. A cela on ajoute 
	// les longueur - 1 espaces 

    x_debut = (240 - longueur_totale_cases) / 2; // Calcul de la position de départ horizontale
	// Ici on calcul la position de depart , 240 - longueur_totale_case donne la longueur restant entre le cote droit et gauche. 
	//C'est pourquo si on veut uniquement le cote gauche on divise par 2
    int i; // Itérateur
    for (i = 0; i < longueur; i++) {
        // Dessiner une case pour chaque lettre du mot
        dessiner_rect(x_debut + i * (20 + espace_entre_cases), 240, 20, 5, 1, 0, Black, Black);
    }
}


void dessiner_lettre_A(int x,int y,short color) {
    
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de droite
	dessiner_rect(x+15,y-20,5,20,1,1,color,color); //Barre de gauche
	dessiner_rect(x,y-20,20,5,1,1,color,color); // On dessin de la barre du haut
	dessiner_rect(x,y-10,20,5,1,1,color,color); // Barre du milieu

}


void dessiner_lettre_L(int x,int y,short color) {
    
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
	dessiner_rect(x,y-5,15,5,1,1,color,color); // On dessine la barre du bas

}

void dessiner_lettre_G(int x,int y,short color) {
    
	dessiner_rect(x+15,y-10,5,10,1,1,color,color); //Barre de droite
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
	dessiner_rect(x,y-20,20,5,1,1,color,color); // On dessine de la barre du haut
	dessiner_rect(x+10,y-10,10,2,1,1,color,color); // Barre du milieu
	dessiner_rect(x,y-5,20,5,1,1,color,color); // On dessine de la barre du bas

}
void dessiner_lettre_O(int x,int y,short color) {
    
	dessiner_rect(x+15,y-20,5,20,1,1,color,color); //Barre de droite
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
	dessiner_rect(x,y-20,20,5,1,1,color,color); // On dessine de la barre du haut
	dessiner_rect(x,y-5,20,5,1,1,color,color); // On dessine de la barre du bas

}
void dessiner_lettre_I(int x,int y,short color) {
    
	dessiner_rect(x+8,y-20,5,20,1,1,color,color); //Barre 

}

void dessiner_lettre_T(int x,int y,short color) {
    

	dessiner_rect(x,y-20,20,5,1,1,color,color); // On dessine de la barre du haut
	dessiner_rect(x+8,y-20,5,20,1,1,color,color); //Barre 


}
void dessiner_lettre_H(int x,int y,short color) {
    
	dessiner_rect(x+15,y-20,5,20,1,1,color,color); //Barre de droite
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
	dessiner_rect(x,y-10,20,5,1,1,color,color); // Barre du milieu

}



void dessiner_lettre_U(int x,int y,short color) {
    
	dessiner_rect(x+15,y-20,5,20,1,1,color,color); //Barre de droite
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
	dessiner_rect(x,y-5,20,5,1,1,color,color); // Barre du milieu

}
void dessiner_lettre_C(int x,int y,short color) {
    
	dessiner_rect(x,y-5,20,5,1,1,color,color); // On dessine de la barre du bas
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
	dessiner_rect(x,y-20,20,5,1,1,color,color); // On dessine de la barre du haut

}

void dessiner_lettre_E(int x,int y,short color) {
    
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
	dessiner_rect(x,y-20,20,3,1,1,color,color); // On dessine de la barre du haut
	dessiner_rect(x,y-3,20,3,1,1,color,color); // On dessine de la barre du bas
	dessiner_rect(x,y-10,20,2,1,1,color,color); // Barre du milieu

}
void dessiner_lettre_S(int x,int y,short color) {
    dessiner_rect(x,y-20,20,3,1,1,color,color); // On dessine de la barre du haut
	dessiner_rect(x,y-20,5,10,1,1,color,color); //Barre de gauche
	dessiner_rect(x,y-12,20,2,1,1,color,color); // Barre du milieu
	dessiner_rect(x+15,y-10,5,10,1,1,color,color); //Barre de droite
	dessiner_rect(x,y-5,20,5,1,1,color,color); // On dessine de la barre du bas
}
void dessiner_lettre_P(int x,int y,short color) {
	dessiner_rect(x,y-20,20,5,1,1,color,color); // On dessine de la barre du haut
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
	dessiner_rect(x+15,y-20,5,10,1,1,color,color); //Barre de droite
	dessiner_rect(x,y-10,20,5,1,1,color,color); // Barre du milieu
}
// void dessiner_lettre_R(int x,int y,short color) {
// 	int y_original = y; //CONSTANTE
// 	int x_original = x; //CONSTANTE
// 	int i; // Iterateur
// 	dessiner_rect(x,y-20,20,5,1,1,color,color); // On dessine de la barre du haut
// 	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
// 	dessiner_rect(x+15,y-20,5,10,1,1,color,color); //Barre de droite
// 	dessiner_rect(x,y-10,20,5,1,1,color,color); // Barre du milieu
// 	//Un R est un P avec la diagonale en plus.
// 	//L'idée pour tracer la diagonale est de tracer plein de petits rectangles de plus en plus baissés ou montés
// 	//Chaque rectangle est petit.
// 	for(i=0;i<20;i++){
// 		if(0<= i < 4){y = y_original - 4;}
// 		else if(4<= i < 8){y = y_original - 3;}
// 		else if(8<= i < 12){y = y_original - 2;}
// 		else if(12<= i < 16){y = y_original - 1;}
// 		else if(16<= i < 20){y = y_original;}
// 		dessiner_rect(x+i,y,1,1,1,1,Black,Black);
// 		// y dépend tout de même de i
// 			}
		
		
// 	}

void dessiner_lettre_R(int x,int y,short color) {
	dessiner_lettre_P(x,y,color);
	dessiner_rect(x+9,y-5,5,5,1,1,color,color);		
	}
void dessiner_lettre_N(int x,int y,short color) {
	dessiner_rect(x,y-20,5,20,1,1,color,color); //Barre de gauche
	 dessiner_rect(x,y-20,5,5,1,1,color,color);
	 dessiner_rect(x+5,y-15,5,5,1,1,color,color);
	 dessiner_rect(x+10,y-10,5,5,1,1,color,color);
	 dessiner_rect(x+15,y-5,5,5,1,1,color,color);
	 dessiner_rect(x+15,y-20,5,20,1,1,color,color); //Barre de droite			
	}
void dessiner_lettre_M(int x,int y,short color) {
	dessiner_rect(x,y-20,3,20,1,1,color,color); //Barre de gauche
	 dessiner_rect(x+3,y-20,3,10,1,1,color,color);
	 dessiner_rect(x+6,y-10,3,10,1,1,color,color);
	 dessiner_rect(x+9,y-10,3,10,1,1,color,color);
	 dessiner_rect(x+12,y-20,3,10,1,1,color,color);
	 dessiner_rect(x+15,y-20,3,20,1,1,color,color); //Barre de droite			
	}




