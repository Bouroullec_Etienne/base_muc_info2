#include "lecture_memoire.h"
#include "globaldec.h" // fichier contenant toutes les dclarations de variables globales
#include <stdio.h>
#include<stdlib.h>
// int ADDR = 0x0A; // ...1010
// uint8_t arr[] = {0x41,0x4C,0x47,0X4F,0X52,0x49,0X54,0x48,0x4D,0x45,0x53,0x55,0x50,0x4E,0x43}; // Exemple de donnes  envoyer ALGORITHMESUPNC
int data_length;
int MSB3;
uint8_t caractere[16]; //Pour que ce soit plus grand que longueur, afin d'éviter tout core dump. 15 lettres
int i;//Iterateur

I2C_M_SETUP_Type masterType;
GPIO_Byte_TypeDef pGPIO;
// PINSEL
PINSEL_CFG_Type pPINSEL;
// Master
I2C_M_SETUP_Type TransferCfg;
//
I2C_TRANSFER_OPT_Type optType;

// config SDA
void config_SDA(){
		pPINSEL.Portnum = PINSEL_PORT_0;
    pPINSEL.Pinnum= PINSEL_PIN_27;
		pPINSEL.Funcnum = 1;
}
// config SCL
void config_SCL(){
		pPINSEL.Portnum = PINSEL_PORT_0;
    pPINSEL.Pinnum= PINSEL_PIN_28;
		pPINSEL.Funcnum = 1;
}

int config_I2C(void){
		PINSEL_ConfigPin(&pPINSEL);// Configurer Pinsel
		config_SDA(); 
		config_SCL(); 
    I2C_Init(LPC_I2C0, 400000); // 400000 = frequence
		
		// Decalage a gauche de 4 bits
		ADDR = (ADDR << 4);// masterType.sl_addr7bit = ...1010 => ADDR = 1010...
		
		// Calcul de MSB3 dynamiquement en se basant sur l alongueur de arr
		data_length = sizeof(arr);
		MSB3 = (data_length - 1) >> 7; // Calculate high bits dynamically
		
		// Update slave address with dynamic MSB3
		masterType.sl_addr7bit = ADDR | MSB3; // Concatenation de ADDR et MSB3

		return masterType.sl_addr7bit;
}

uint8_t* configMaster_reader(uint8_t * adresse,int longueur){
  //Ici, on adapte le code pour qu'il soit modulaire. Même si pour le pendu la longueur sera toujours de 1. 
  //Pour que ce soit plus grand que longueur, afin d'éviter tout core dump.
  
    TransferCfg.sl_addr7bit = config_I2C(); // Adresse de l'esclave
    TransferCfg.tx_data = adresse;
    TransferCfg.tx_length = 1; // La longueur a ecrire,
    for(i=0;i<longueur;i++){
      TransferCfg.rx_data = &caractere[i]; //size de arr + 1, car le premier element ne sert que a la page 
      TransferCfg.rx_length = 1; // La longueur a lire, la taille de ce qu'on veut recevoir.
      I2C_MasterTransferData(LPC_I2C0, &TransferCfg,optType); //On veut lire dans la memoire LPC_I2C0,
                                                                                                               
  }
  return caractere;	 
}

// void write_I2C(uint8_t* car){
// 	configMaster(uint8_t* car);
// }

uint8_t * read_I2C(uint8_t addr,int length){
  //Le principe est: Ecrire une adresse sur le bus I2C.
	//Le micro-controleur comprendras qu'il faut lire à cet endroit,
	//C'est pour cela que il faut le reconfigurer pour de l'écriture.
	uint8_t * tab_c = configMaster_reader(&addr,length);

  return tab_c;
  //Une fois retourné, le main sera chargé de lire le char*
}

