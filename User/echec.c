#include "echec.h"

void T0_init() {

		TIM_TIMERCFG_Type pTimerCfg;
		TIM_MATCHCFG_Type pMatchCfg;
	  pTimerCfg.PrescaleOption =  TIM_PRESCALE_USVAL; // Prescale in muS
    pTimerCfg.PrescaleValue = 200;   // Prescale value
    TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &pTimerCfg); // Initialiser Timer 0
    
		pMatchCfg.MatchChannel = 0; 
	  pMatchCfg.MatchValue= 15;     // Match value = demi_periode 1 / 2F 
	  pMatchCfg.IntOnMatch = ENABLE;   // interruption en Match
	  pMatchCfg.StopOnMatch = DISABLE;   // interruption en Match
	  pMatchCfg.ResetOnMatch = ENABLE;   // interruption en Match
    pMatchCfg.ExtMatchOutputType = TIM_EXTMATCH_TOGGLE;
	
		TIM_ResetCounter(LPC_TIM0);
    TIM_ConfigMatch(LPC_TIM0, &pMatchCfg); // Configure match settings
}

// TIMER1
void T1_init() {
    TIM_TIMERCFG_Type pTimerCfg;
    TIM_MATCHCFG_Type pMatchCfg;

    // Configuration du Timer 1
    pTimerCfg.PrescaleOption = TIM_PRESCALE_USVAL; // Utilisation de microsecondes pour la pr�division
    pTimerCfg.PrescaleValue = 1000; // Pr�division par 1000 (1 kHz)

    TIM_Init(LPC_TIM1, TIM_TIMER_MODE, &pTimerCfg); // Initialisation du Timer 1

    // Configuration du Match (Interruption sur Match)
		pMatchCfg.MatchChannel = 1; 
		pMatchCfg.MatchValue = 1000; // Valeur de correspondance (1 seconde avec pr�division de 1000)
    pMatchCfg.IntOnMatch = ENABLE; // Activation de l'interruption sur match
    pMatchCfg.StopOnMatch = ENABLE; // Ne pas arr�ter le timer sur match
    pMatchCfg.ResetOnMatch = ENABLE; // R�initialiser le compteur sur match

    TIM_ConfigMatch(LPC_TIM1, &pMatchCfg); // Configuration des param�tres de match
    NVIC_EnableIRQ(TIMER1_IRQn); // Activation de l'interruption du Timer 1

}

void echec_sound(void) {
		PINSEL_CFG_Type pPINSEL;

		//////////////////
		// Configurtion du TIMER0
    PINSEL_ConfigPin(&pPINSEL); // Configuration du Pinsel
		pPINSEL.Portnum = PINSEL_PORT_1;
    pPINSEL.Pinnum= PINSEL_PIN_28;
		pPINSEL.Funcnum = 3;
    PINSEL_ConfigPin(&pPINSEL);// Configurer Pinsel
    T0_init(); // Initialiser timer0
		T1_init();
	
		TIM_Cmd(LPC_TIM0, ENABLE); // Lance le Timer0    while (1) 
	  TIM_Cmd(LPC_TIM1, ENABLE); // Activation du Timer 1
	
    while(1){
			
		}
}
void TIMER1_IRQHandler() {
		TIM_Cmd(LPC_TIM0, DISABLE);
    TIM_ClearIntPending(LPC_TIM1, TIM_MR1_INT); // Nettoyage du drapeau d'interruption
		
}

