/*
Fonction permettant de dessiner un rectangle de coordonn�es (x,y)
de longueur lng, de largeur lrg, d'�paisseur de trait de bordure e,
plein ou vide, de couleur de bordure e_color et de fond bg_color
*/
 void dessiner_rect(unsigned int x, unsigned int y, unsigned int lng, unsigned int lrg, unsigned int e, unsigned short plein, unsigned short e_color, unsigned short bg_color);
 void dessiner_ligne(unsigned int x, unsigned int y, unsigned int l,unsigned int e, char orientation, unsigned short color);
 void dessiner_bonhomme();
 void afficher_lettre_A();
 void dessiner_pendouilloire();
 void dessiner_une_lettre(char,int);
void dessiner_cases(char []);
void dessiner_lettre_A(int,int,short);
void dessiner_lettre_L(int,int,short);
void dessiner_lettre_G(int,int,short);
void dessiner_lettre_O(int,int,short);
void dessiner_lettre_I(int,int,short);
void dessiner_lettre_T(int,int,short);
void dessiner_lettre_H(int,int,short);
void dessiner_lettre_U(int,int,short);
void dessiner_lettre_C(int,int,short);
void dessiner_lettre_E(int,int,short);
void dessiner_lettre_S(int,int,short);
void dessiner_lettre_P(int,int,short);
void dessiner_lettre_R(int,int,short);
void dessiner_lettre_N(int,int,short);
void dessiner_lettre_M(int,int,short);